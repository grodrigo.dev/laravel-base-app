# Laravel app with basic crud and auth

>>>
On this example we run docker to startup the application, and 
the Laravel source code will be in the src folder.  
If you got another method go for it, this is just a suggestion.
>>>

To use this app you can use docker from another project and put this code inside of it:  
```bash
mkdir laravel-base-app
cd laravel-base-app
git clone https://gitlab.com/grodrigo.dev/laravel.git .
# and put the content of the laravel project into the src folder
rm -rf src/*
rm -rf src/.*
cd src
git clone https://gitlab.com/grodrigo.dev/laravel-base-app.git
## copy or edit laravel env example
cp .env.example .env
cd ..
## copy or edit docker .env example
cp .env.example .env
docker-compose up -d
docker-compose exec app composer install
docker-compose exec app php artisan key:generate
docker-compose exec app npm install
docker-compose exec app php artisan migrate
```
>>>
check your browser on  
localhost:8085
>>>

## To enter into the container
```
docker-compose exec app bash
# then, inside of it you can run
$ php artisan 
```

## To run the tests do
$ php artisan test
