@extends('enterprises.layout')

@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Home</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-success" href="{{ route('enterprises.create') }}"> Create New Enterprise</a>
            </div>
        </div>
    </div>

    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif

    <table class="table table-bordered">
        <tr>
            <th>No</th>
            <th>Name</th>
            <th>Url</th>
            <th>Description</th>
            @if (Auth::check())
            <th width="280px">Action</th>
            @endif
        </tr>

        @foreach ($enterprises as $enterprise)
            <tr>
                <td>{{ ++$i }}</td>
                <td>{{ $enterprise->name }}</td>
                <td>{{ $enterprise->url }}</td>
                <td>{{ $enterprise->description }}</td>

                @if (Auth::check())
                <td>
                    <form action="{{ route('enterprises.destroy', $enterprise->id) }}" method="POST">
                            <a class="btn btn-info" href="{{ route('enterprises.show', $enterprise->id) }}">Show</a>
                            <a class="btn btn-primary" href="{{ route('enterprises.edit', $enterprise->id) }}">Edit</a>
                            @csrf
                            @method('DELETE')
                        <button type="submit" class="btn btn-danger">Delete</button>
                    </form>
                </td>
                @endif

            </tr>
        @endforeach

    </table>



    {!! $enterprises->links() !!}



@endsection
