<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\User;
use Illuminate\Support\Facades\Auth;

class SubmitEnterprisesTest extends TestCase
{
    use RefreshDatabase;

    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testExample()
    {
        $response = $this->get('/');

        $response->assertStatus(200);
    }

    function guest_cant_submit_a_new_enterprise()
    {
        $response = $this->post('/enterprises', [
            'name' => 'Example Enterprise',
            'url' => 'http://example.com',
            'description' => 'Example description',
        ]);

        $this->assertDatabaseMissing('enterprises', [
            'name' => 'Example Enterprise'
        ]);

        $response
            ->assertStatus(302)
            ->assertHeader('Location', url('/login'));
    }

    public function test_user_can_view_a_login_form()
    {
        $response = $this->get('/login');

        $response->assertSuccessful();
        $response->assertViewIs('auth.login');
    }

    public function test_user_cannot_view_a_login_form_when_authenticated()
    {
        $user = factory(User::class)->make();

        $response = $this->actingAs($user)->get('/login');

        $response->assertRedirect('/home');
    }

    function registeredUser_can_submit_a_new_enterprise()
    {
        $user = factory(User::class)->make();

        $response = $this->actingAs($user)->post('/enterprises', [
            'name' => 'Example Enterprise',
            'url' => 'http://example.com',
            'description' => 'Example description.',
        ]);

        $this->assertDatabaseHas('enterprises', [
            'name' => 'Example Enterprise'
        ]);

        $response
            ->assertStatus(302)
            ->assertHeader('Location', url('/enterprises'));

        $this
            ->get('/')
            ->assertSee('Example Enterprise');
    }

}
